
function ticta--dynamic-window-title
  if ticta-cfg assert --not enable_dynamic_window_title
    return 0
  end

  # Current task
  set task

  # " on " or " in "
  set task_preposition
  if test -n "$ticta__dynamic_window_title_task"
    set task "\"$ticta__dynamic_window_title_task\""
    set task_preposition " on "
  end

  set state
  if test -n "$argv[1]"
    set state "$argv[1]"
  else
    set state ( ticta--prompt-pwd --no-color --short-more -l 2 -s 2 )
  end

  set host
  if set -q SSH_CONNECTION
    set host ( id --user --name )@( ticta-get-hostname )
  end 

  if ticta--status-is-in-tmux; and ticta-cfg assert enable_tmux_dynamic_window_title_integration
    test -n "$task"; and set task_preposition " in "
    tmux rename-window "$task$task_preposition$state"
  end
  
  echo "$task$task_preposition$host $state"
end

function set-task
  argparse 'i/interactive' -- $argv

  test -n "$_flag_i"; and read -P "Task: " argv

  if test -n "$argv"
    set -gx ticta__dynamic_window_title_task $argv
  else
    set -e ticta__dynamic_window_title_task
  end
end
complete -x -c set-task
