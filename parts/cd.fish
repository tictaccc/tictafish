for n in ( seq 1 10 )
  set cmd "cd "( string repeat -n $n "../" | string sub --end -1 )
  alias "."( string repeat -n $n "." )=$cmd
  alias "..$n"=$cmd
end

function cdl
  cd $argv
  ls
end
