
function ticta--prompts-block
  function l2
    echo -n $bg_color_accent">"$bg_normal" "
  end
  function l3
    echo -e $bg_color_accent $USER" "$hostname" "$bg_normal
  end

  echo $bg_color_accent ( ticta--prompt-pwd --color-dir $bold$white --color-seperator $normal$bg_color_accent$white )" "$bg_normal
  if set -q prompt_demo
    l2
    echo
    l3
  else
    echo
    l3
    echo -ne "\033[2A"
    l2
  end

end
