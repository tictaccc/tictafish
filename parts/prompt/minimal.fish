
function ticta--prompts-minimal
  echo ( basename $PWD )
  echo -n $color_accent
  echo -n $USER
  test "$last_status" != "0"; and echo -n " "$bg_brred$brwhite$last_status
  echo -n $normal
  echo -n ' > '
end
