
function ticta--prompts-inline-block
  set -a parts
  for integration in $integrations;
    eval $split_integration
    switch $name
    case "git"
      set -a parts $normal$default_output
    # case "nvm"
      # set -a parts $bold$bg_brblue" node "$state""
    case '*'
      set -a parts $normal$invert(echo $default_output | string trim)
    end
  end

  # if [ $nvm_system = 0 ]
  #   set -a parts $bg_brblue$white$bold" "( nvm current | sed -e 's/v/node /' )
  # end

  echo -n $bold
  if [ (count $parts) -ne 0 ]
    # echo -n " "
    for part in $parts
      echo -n $part$normal" "
    end
    echo
  end

  if [ $last_status != 0 ]
    echo -n $bg_color_tertiary $last_status" :( "$bg_black""
  else
    echo -n $black
  end

  echo -n $bg_black$white" "$USER" "
  echo -n $bg_color_accent$white ( ticta--prompt-pwd --color-dir $white --color-seperator $white )" "$bg_normal$color_accent" "$normal
end
