
function ticta--prompts-bash
  echo -n [$USER"@"$hostname ( ticta--prompt-pwd --color-dir $normal --color-seperator $normal --short-more -l 3 -s 1 )]
  [ $last_status != 0 ]; and echo -n "$last_status"
  [ ( id -u ) = 0 ]; and echo -n "# "; or echo -n "\$ "
end
