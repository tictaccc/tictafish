
function ticta--prompts-bashplus
  set -l a $color_accent
  set -l b $color_tertiary

  for integration in $integrations;
    eval $split_integration
    switch $name
    case '*'
      echo -n $normal$default_output" "
      # set -a parts $normal$invert$name"->"$state $normal
    end
  end

  if test -n "$integrations"
    echo $normal
  end

  echo -n $b"["$a$USER$b"@"$a$hostname ( ticta--prompt-pwd --color-dir $normal --color-seperator $normal --short-more -l 3 -s 1 )$b"]"$a
  [ $last_status != 0 ]; and echo -n " $red$last_status$a "
  [ ( id -u ) = 0 ]; and echo -n "#"; or echo -n '$'
  echo -n $normal" "
end
