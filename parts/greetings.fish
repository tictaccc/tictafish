function ticta--greeting
  set suffixed_day ( tictabox daysuffix )

  switch (echo $argv[1])
  case "none"
  case "hi-block"
    set -l color ( ticta-cfg get color_accent )
    set color $$color
    set -l bg_color bg_( ticta-cfg get color_accent )
    set bg_color $$bg_color
    echo -n $color$bg_normal" 🭅"$white$bg_color" hi! it's "$bold( date "+%-I:%M %p" )$normal$bg_color" on "$bold( date "+%b" ) $suffixed_day$bg_normal$color"🭡"$normal

  case "hi!" "*"
    echo -n $white"~ hi "$blue$USER$white"! it's "$magenta( date "+%-I:%M %p" )$white" on "$yellow( date "+%b" ) $suffixed_day$white"! ~"

  end
end
