
function ticta--status-is-in-tmux
  if set -q TMUX TMUX_PANE
    return 0
  end
  return 1
end

function ticta--status-is-fresh-tmux-session
  # if test ( tmux list-sessions | count ) = "1" \
  #   -a ( tmux list-windows | count ) = "1" \
  #   -a ( tmux list-panes | count ) = "1"
  return 0
end

function ticta--tmux-init
  if ticta-cfg assert enable_tmux_TERM_matching
    and set -q ticta__tmux_host_term
    and test "$TERM" !=  "$ticta__tmux_host_term";

    set -g host_term_colors ( tput -T $ticta__tmux_host_term colors 2> /dev/null )
    set -g term_colors ( tput -T $TERM colors 2> /dev/null )

    if test "$host_term_colors" != "$term_colors"
      set -l color_suffix "-"$host_term_colors"color"

      if tput -T $TERM$color_suffix colors &> /dev/null
        set -gx TERM $TERM$color_suffix
      else
        set -gx TERM tmux$color_suffix
      end
      ticta-sync-colors
    end
  end

  if ticta-cfg assert enable_tmux_configuration
    tmux set-option -g status-bg ( 
      ticta--resolve-color --hex-prefix '#' ( ticta-cfg get color_tmux_status ) \
      | string replace --regex --ignore-case '^br(\w+)$' 'bright$1' 's/br(\w+)/bright\1/' 
    )
  end
    
  if ticta-cfg assert enable_tmux_dynamic_window_title_integration
    tmux set-option -g set-titles on
    tmux set-option -g set-titles-string "[#{session_name}] #{pane_title}"
  end
end

if status is-interactive
  if ticta--status-is-in-tmux
    ticta--tmux-init
  else
    set -gx ticta__tmux_host_term $TERM
  end
end
