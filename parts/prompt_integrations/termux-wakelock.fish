
function ticta--prompt-integration-check-termux-wakelock
  if not type -q termux-wake-lock
    return 1
  end

  function termux-get-wakelock-enabled
    if not set -l notifications ( timeout 2.9s termux-notification-list )
      alias termux-get-wakelock-enabled='return 1'
      return 1
    end

    if echo $notifications | jq -r '.[] | select(.packageName == "com.termux") | select(.id == 1337).content ' | grep -iq 'wake lock held'
       # echo 'locked'
       return 0
     else
       # echo 'unlocked'
       return 1
    end
  end
end

function ticta--prompt-integration-termux-wakelock
  set -l state ( termux-get-wakelock-enabled; and echo 'locked'; or echo 'unlocked' )

  echo $state

  test $state = "locked"; and echo -n $normal$brgreen; or echo -n $normal$bg_red
  echo -n "[wake-$state]"
end
