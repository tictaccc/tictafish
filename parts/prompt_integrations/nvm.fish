
function ticta--prompt-integration-check-nvm
  if type -q nvm; and not string match -irq 'this is not the package.+' ( nvm current )
    return 0
  end
  return 1
end

function ticta--prompt-integration-nvm
  set -l nvm_current ( nvm current )

  if [ -z "$nvm_current" -o "$nvm_current" = "" -o "$nvm_current" = "system" ]
    return 1
  end

  set -l state (echo $nvm_current | sed -e 's/v//' | string trim)

  echo $state
  echo $brblue'['$blue"node "$state$brblue']'
end
