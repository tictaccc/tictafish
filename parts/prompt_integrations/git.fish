function ticta--prompt-integration-check-git
  return 0
end

function ticta--prompt-integration-git
  set -l fish_git_output ( __fish_git_prompt "(%s)" | string trim )

  test -z "$fish_git_output"; and return 1

  echo $fish_git_output
  echo $fish_git_output
end
