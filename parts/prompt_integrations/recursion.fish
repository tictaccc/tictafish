function ticta--prompt-integration-check-recursion
  if set -q TICTA_DEBUG
    return 0
  end
  return 1
end

function ticta--prompt-integration-recursion
  test $ticta__recursion_n -eq 0; and return 1

  echo $ticta__recursion_n
  echo $brred"[⟳ $bold$ticta__recursion_n$unbold"$brred"]"
end
