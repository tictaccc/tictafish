function ticta--prompt-integration-check-python-venv
end

function ticta--prompt-integration-python-venv
  if test -z "$VIRTUAL_ENV"
    return 1
  end

  echo $VIRTUAL_ENV
  echo "$white($cyan"(basename "$VIRTUAL_ENV")"$white)"
end
