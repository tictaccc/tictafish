function ticta--prompt-integration-check-screen
  if test -z "$STY"
    return 1
  end
  return 0
end

function ticta--prompt-integration-screen
  if test -z "$STY"
    return 1
  end

  set -l pid ( string split '.' $STY )[1]
  set -l session_name ( screen -list | grep "$pid" | perl -pe 's/\s*\d+\.(\S+).*/\1/' )

  if test -z "$session_name" -o ( string sub --length 3 "$session_name" ) = 'pts'
    set session_name "screen $pid"
  else
    set session_name "$bold$session_name$unbold"
  end

  echo $session_name
  echo $bryellow"[$session_name]"
end
