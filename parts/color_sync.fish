# http://xtermjs.org/docs/api/vtfeatures/#set-or-query-default-foreground-color
# https://iterm2.com/documentation-escape-codes.html
# https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797

function ticta--sync-colors
  ticta--debug "syncing"
  if ticta-cfg assert use_shell_colors
    for name in $color_list br$color_list
      set -g ticta__named_color_$name $name
    end
  else
    for name in $color_list br$color_list
      set -g ticta__named_color_$name ( ticta--resolve-color $name )
      set -g fish_color_$name ( ticta--resolve-color $name )
    end
  end

  for name in $color_list br$color_list
    set -l named_color_var ticta__named_color_$name 

    set -g $name    ( set_color "$$named_color_var" $name )
    set -g bg_$name ( set_color --background "$$named_color_var" --background $name )

    set -g fish_color_$name ( ticta--resolve-color $name )
  end
end

function ticta--sync-colors--fish
  if ticta-cfg assert overwrite_fish_colors
    set -l list ( set | grep '^tdcfg_fish_c' | sed -E 's/tdcfg_fish_c_([a-zA-Z]+).*/\1/' )
    for name in $list
      # set fish_color_$name ( ticta-cfg get fish_c_$name )
      set fish_color_$name ( ticta-cfg get fish_c_$name )
    end
  end
end

function ticta--sync-colors--tty
  if not tty | string match '/dev/tty*' || ticta-cfg assert --not enable_nice_tty
    return 0
  end

  set -l colors
  set -al colors \
  '0 black' \
  '8 brblack' \
  '1 red' \
  '9 brred' \
  '2 green' \
  'A brgreen' \
  '3 yellow' \
  'B bryellow' \
  '4 blue' \
  'C brblue' \
  '5 magenta' \
  'D brmagenta' \
  '6 cyan' \
  'E brcyan' \
  '7 white' \
  'F brwhite'

  # 0 is bg
  echo -ne \e"]P00C0C0C"
  for color in $colors
    echo $color | read id name
    echo -ne \e"]P$id"( ticta--resolve-color $name | string trim -l -c '#' )
  end
end

function ticta--sync-colors--xterm
  if ticta-cfg assert --not enable_terminal_color_sync
    return
  end

  set output
  for i in ( seq 0 7 )
    set -l i_br ( math $i + 8 )
    set -l name $color_list[( math $i + 1 )]
    set output "$output\e]4;$i;"( ticta--resolve-color $name )"\a"
    set output "$output\e]4;$i_br;"( ticta--resolve-color br$name )"\a"
  end
  
  echo -en "$output"
end

function ticta-sync-colors -d "Sync fish and color variables with the current color scheme"
  ticta--sync-colors
  ticta--sync-colors--fish
  ticta--sync-colors--tty
  ticta--sync-colors--xterm

  function _set_color
    set -l color $argv[1]

    set -l arg2 $argv[2]
    test -z $arg2; and set arg2 0
    set -l background ( [ $arg2 = "1" ]; and echo 1; or echo 0 )
    for name in $color_list
      if [ $color = $name ]
        if [ $background = "1" ]
          set color bg_$color
        end

        echo $$color
        return 0
      end
    end

    if echo $color | grep -q -E '^[a-zA-Z0-9]+'
      if [ $background = "1" ]
        echo ( set_color --background $color )
      else
        echo ( set_color $color )
      end
    else
      if [ $background = 1 ]
        set color bg_$color
      end
      echo $$color
    end
  end

  set -g color_accent ( _set_color ( ticta-cfg get color_accent ) )
  set -g bg_color_accent ( _set_color ( ticta-cfg get color_accent ) 1 )

  set -g color_tertiary ( _set_color ( ticta-cfg get color_tertiary ) )
  set -g bg_color_tertiary ( _set_color ( ticta-cfg get color_tertiary ) 1 )
end
