
function ticta--prompt-pwd
  argparse \
  'color-dir=' \
  'color-seperator=' \
  'color-home-char=' \
  'l/long=' \
  's/short=' \
  'home-char=' \
  'short-more' \
  'no-color' \
  -- $argv

  set -l color_dir       ""
  set -l color_seperator ""
  set -l color_home_char ""
  if test -z $_flag_no_color
    set color_dir       ( default-val $_flag_color_dir $brgreen )
    set color_seperator ( default-val $_flag_color_seperator $green )
    set color_home_char ( default-val $_flag_color_home_char $color_dir )
  end

  set -l n_long ( default-val $_flag_long 3 )
  set -l n_short ( default-val $_flag_short 3 )

  set -l home_char ( default-val $_flag_home_char '⌁')
  # set -l short_more ( default-val $_flag_short_more '' )

  switch "$PWD"
  case "$HOME"
    echo $color_home_char$home_char
    return 0
  case "/"
    echo $color_seperator/
    return 0
  end


  set -l in_home ( pwd | grep -qE "^$HOME"; and echo 1; or echo 0 )
  set -l cwd_str ( pwd | sed -E "s=^$HOME==" | string trim -l -c '/' )

  set -l dirs ( string split '/' $cwd_str )[-1..1]
  set -al output

  set -l i 0
  for dir in $dirs
    if test $n_long -gt 0
      set -a output $dir
      set n_long ( math $n_long - 1 )

    else if test $n_short -gt 0
      set -a output ( string sub -l ( string-startswith '.' $dir; and echo 2; or echo 1 ) $dir )
      set n_short ( math $n_short - 1 )

    else
      set -l n ( math ( count $dirs ) - $i )
      if test -z $_flag_short_more
        set -a output "..$n more.."
      else
        set -a output "+$n"
      end
      break
    end

    set i ( math $i + 1 )
  end

  set output $output[-1..1]

  set -l out_str ( test $in_home -eq 1; and echo $color_home_char$home_char )
  for s in $output
    set out_str "$out_str$color_seperator/$color_dir$s"
  end

  set out_str $out_str$color_seperator/
  if test -z $_flag_no_color
    set out_str $out_str$normal
  end

  echo -en "$out_str"
end
