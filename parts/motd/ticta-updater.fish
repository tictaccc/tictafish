
function ticta--motd--ticta-updater
  if ticta-updater is_updated
    return
  end

  set code "$red`$normal$bg_black"
  set code_off "$bg_normal$red`$normal"

  echo "notice"
  if ticta-cfg assert enable_auto_update
    echo "ticta.fish auto update will run at next shell spawn."
    echo "To disable this, run " \
         "$code""ticta-cfg save enable_auto_update 0""$code_off"
    else
      echo "ticta.fish update waiting."
      echo "Update with $code""ticta-updater run""$code_off"
  end
end