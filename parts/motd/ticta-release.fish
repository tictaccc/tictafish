
function ticta--motd--ticta-release
  set dim ( tput dim )
  set green ( set_color green )
  set normal ( set_color normal )
  
  set style_parenthesis "$normal$dim"
  set style_version "$normal$green"
  
  # I'm not a fan of this but it works well.
  set start_pwd $PWD
  cd $ticta__dir
  
  set branch ( git rev-parse --abbrev-ref HEAD )
  set local  ( git rev-parse HEAD | string sub --length 7 )
  set remote ( git rev-parse origin/$branch 2> /dev/null | string sub --length 7 )
  or set local_branch
  git show --no-patch --format=%ci HEAD | read local_date local_time

  cd $start_pwd
  
  # set message "ticta.fish $green$TICTA_VERSION ($local)$normal on $green$release$normal"
  set message "ticta.fish $style_version$local_date"
  set -a message "$style_parenthesis($style_version$local$style_parenthesis)$normal"

  if test "$branch" != "main"
    set -a message "on $style_version$branch"
  end

  if set -q local_branch
    set -a message "$dim(local branch)$normal"
  end

  echo "info"
  echo $message
end
