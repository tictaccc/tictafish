
set -g ticta__prompt_integrations

function ticta--load-prompt-integrations
  for path in $argv
    if test -d "$path"
      set files "$path/"*
    else if test -e "$path"
      set files "$path"
    else
      return 1
    end

    for fn in $files
      set name ( basename $fn .fish )

      source $fn

      set check_func ticta--prompt-integration-check-$name

      if functions -q "$check_func" && not $check_func
        continue
      end
      
      set -a ticta__prompt_integrations "$name"
    end
  end
end

function ticta--prompt-get-integrations
  set -l timeout ( ticta-cfg get prompt_integration_timeout_seconds )"s"
  set -l disabled_integrations ( ticta-cfg get disabled_prompt_integrations )

  set -al integrations

  for integration in $ticta__prompt_integrations
    if contains $integration $disabled_integrations
      continue
    end

    set -l func_name ticta--prompt-integration-$integration

    set output ( $func_name )
    set -l res $status

    if test $res -eq 0
      set -a integrations "$integration\x01$output[1]\x01$output[2]"
    end
  end

  for integration in $integrations
    echo -e $integration
  end
end

ticta--load-prompt-integrations \
  $ticta__dir/parts/prompt_integrations/ \
  $HOME/.config/fish/ticta/prompt-integrations.d

