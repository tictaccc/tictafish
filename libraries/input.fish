# provides read-mode-on
# provides read-mode-off
# provides read-buffer consumes all input currently in stdin. blocks until input is available. See read-mode-[on/off] for better performance

set -g ticta__in_char_read_mode 0

function ticta--lib-function-read-mode-on
  stty -icanon -echo
  set -g ticta__in_char_read_mode 1
end

function ticta--lib-function-read-mode-off
  stty icanon echo
  set -g ticta__in_char_read_mode 0
end

function ticta--lib-function-read-buffer
  argparse 'm/max-blocksize=' 't/timeout=' -- $argv

  set -l char
  set -l timeout_cmd

  if test -n "$_flag_timeout"
    set timeout_cmd "timeout --foreground $_flag_timeout"
  end

  # echo timeout: $timeout_cmd

  test $ticta__in_char_read_mode -eq 0; and stty -icanon -echo
  if test -n "$_flag_max_blocksize"
    set char ( eval $timeout_cmd dd if=/dev/stdin bs=$_flag_max_blocksize count=1 2> /dev/null )
  else
    set char ( eval $timeout_cmd dd if=/dev/stdin bs=512 count=1 2> /dev/null )
  end
  set cmd_status $status
  test $ticta__in_char_read_mode -eq 0; and stty icanon echo

  if test $cmd_status -eq 124
    return 124
  else if test $cmd_status -ne 0
    return $cmd_status
  else if test -z "$char" -o "$char" = \n -o "$char" = \r
    printf '\\\n'
  else
    printf $char
  end
  return 0

end
