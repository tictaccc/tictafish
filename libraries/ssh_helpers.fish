# provides ssh-tunnel

function ticta--lib-function-ssh-tunnel
  argparse "h/help" "l/local=" "r/remote=" "c/compression" "H/host=" \
           "i/interactive" -- $argv

  if set -q _flag_h
    echo "This is just a fancy alias for `ssh -L LOCAL_PORT:localhost:REMOTE_PORT user@hostname`"
    echo
    echo "Usage:"
    echo "  ssh-tunnel -r REMOTE_PORT [-l LOCAL_PORT] [-ci] user@hostname"
    echo
    echo "Options:"
    echo "  -r, --remote        Remote port to be forwarded to --local"
    echo "  -l, --local         Local port. (Default: REMOTE_PORT)"
    echo "  -c, --compression   Enable SSH gzip compression"
    echo "  -i, --interactive   Disable SSH -N flag, enabling normal ssh term access as well"
    # echo "  -H, --host          SSH 'destination' value"
    return 255
  end

  if not set -q _flag_remote
    echo "Remote port required. See ssh-tunnel -h" > /dev/stderr
    return 2
  end

  if not set -q argv[1]
    echo "Host argument required. See ssh-tunnel -h" > /dev/stderr
    return 2
  end

  set host ( string join " " $argv )

  set port_remote $_flag_remote
  set port_local ( set -q _flag_local; and echo $_flag_local; or echo $port_remote)

  set flag_compression ( set -q _flag_compression; and echo true; or echo false )
  set flag_interactive ( set -q _flag_interactive; and echo true; or echo false )

  echo "Opening ssh tunnel"
  echo -n "localhost:"
  echo -n $bold$port_local$normal
  echo -n " <-> "
  echo -n $host":"
  echo    $bold$port_remote$normal

  ssh \
  -L $port_local:localhost:$port_remote \
  ( [ $flag_compression = 'true'  ]; and echo "-C") \
  ( [ $flag_interactive = 'false' ]; and echo "-N") \
   $host
end

# complete -c ssh-tunnel -f

alias _complete="complete -x -c ticta--lib-function-ssh-tunnel"
_complete
_complete -s h -l help   -d "help"
_complete -s l -l local  -d "local port"
_complete -s r -l remote -d "remote port"
_complete -s c -l compression -d "enable ssh compression"
functions -e _complete
# complete -x -c ssh-tunnel -a

# tictabox-register ssh-tunnel -d "Shortcut to open ssh port forwarding tunnel"
