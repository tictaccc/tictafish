#provides new-dot-desktop (Depreciated) Desktop entry wizard

function ticta--lib-function-new-dot-desktop
  if test -z $argv[1]
    echo "Please provide a file name, without the .desktop extension (it'll be appended automatically)"
    exit 1
  end

  set target $argv[1]".desktop"
  set name ( basename $target .desktop )

  set normal ( set_color normal )

  function print
    echo $argv > /dev/stdin
  end

  set filename ( set_color white --background brgrey )
  set -g c_choice_key ( set_color green )

  function choice_str
    echo "Choose an option for the "$c_choice_key$argv[1]$normal" key"
  end

  function choice
    set c_choice_i ( set_color brred )
    print "Choose an option for the "$c_choice_key$argv[1]$normal" key"
    sleep 3
    # echo "default: "$c_choice_i"1"$normal
    set -l i 0
    for arg in $argv
      if test $i -eq 0
        set i 1
        continue
      end

      print -n $c_choice_i
      print $c_choice_i"["$i"]"$normal $arg
      set i (expr $i + 1)
    end
    print $c_choice_i"[... Or type in your own]"
    set input ( read -P "Number or custom value (def: "$c_choice_i"1"$normal")> " )

    if test -z $input
      echo $argv[2]
    else if test $input -gt 0 2> /dev/null
      echo $argv[(expr $input + 1)]
    else
      echo $input
    end
  end

  # choice StringChoice hello worlddd its meeeee
  # choice Type Application Link Directory
  # exit

  if not choice_yesno -p "Writing to "$filename$target$normal", is that ok?"
    return 1
  end

  # read -P "> " -n 1 > /dev/null

  if test -e $target
    echo "File already exists :("
    switch ( choice_list -p "What do you want to do?" -c "Cancel" -c "Overwrite" -c "Append")
    case Cancel
      return 1
    case Overwrite
      echo -n "" > $target
    case Append
    case *
      echo "Invalid choice"
    end
  end

  echo "[Desktop Entry]" >> $target
  echo "Exec="( read -P "Exec string > " ) >> $target
  # echo "Name="( choice Name $argv[1] ) >> $target
  echo "Name="( choice_list -p (choice_str Name) -c $name ) >> $target
  echo

  # echo "Type="( choice Type Application Link Directory ) >> $target
  echo "Type="( choice_list -p (choice_str Type) -c Application -c Link -c Directory ) >> $target
  echo

  # echo "Icon="( choice Icon $argv[1] "") >> $target
  echo "Icon="( choice_list -p (choice_str Icon) -c $name "") >> $target
  echo

  echo "Comment=" (read -P "Comment > ") >> $target
  echo ""
end
