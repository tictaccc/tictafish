# provides random-string

set -g ticta__letters_lower a b c d e f g h i j k l m n o p q r s t u v w x y z
set -g ticta__letters_upper A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
set -g ticta__letters_all $ticta__letters_lower $ticta__letters_upper

function ticta--lib-function-random-string
  argparse 'n/length=' 'l/lower' 'u/upper' 'a/all' -- $argv

  set -l letters

  if test -n "$_flag_all" -o \( -n "$_flag_lower" -a -n "$_flag_upper" \)
    set letters $ticta__letters_all
  else if test -n "$_flag_upper"
    set letters $ticta__letters_upper
  else
    set letters $ticta__letters_lower
  end

  if test -z "$_flag_length"
    random choice $letters
  else if not tictabox is-number "$_flag_length"
    echo "invalid length: \"$_flag_length\"" > /dev/stderr
    exit 1
  else
    set -l res
    set -l i 0
    while test $i -lt $_flag_length
      # set res "$res"( random choice $letters )
      echo -n ( random choice $letters )
      set i ( math $i + 1 )
    end
    echo -n $res
  end
end
