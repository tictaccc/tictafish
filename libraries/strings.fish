# name strings
# provides string-clean (WIP) Removes most terminal escapes from string
# provides string-clean-len Gets length of `string-clean` output
## provides string-startswith Asserts if string starts with another string
## provides string-endswith Asserts if string ends with another string

function ticta--lib-function-string-clean
  echo "$argv" | sed 's/\x1b\[[0-9;]*m//g'
end
complete -x -c ticta--lib-function-string-clean

function ticta--lib-function-string-clean-len
  string length -- ( string-clean "$argv" )
end
complete -x -c ticta--function-string-clean-len


# function ticta--lib-function-string-startswith
#   set match "$argv[1]"
#   set string ( string join " " -- $argv[2..] )
#
#   if test "$match" = ( string sub -l ( string length -- "$match" ) -- "$string" )
#     return 0
#   end
#   return 1
# end
# complete -x -c ticta--lib-function-string-startswith
#
# function ticta--lib-function-string-endswith
#   set match "$argv[1]"
#   set string ( string join " " -- $argv[2..] )
#
#   if test "$match" = ( string sub --end -( string length -- "$match" ) -- "$string" )
#     return 0
#   end
#   return 1
#
# end
# complete -x -c ticta--lib-function-string-endswith
