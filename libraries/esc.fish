# provides esc Terminal escape code toolbox

complete -x -c ticta--lib-function-esc -k -a "h help smcup cursor-mode-on rmcup cursor-mode-off getpos \
cpos home goto up down right left setcolumn clearscren clearline show visible \
cvvis hide invisible civis savepos restorepos"

function ticta--lib-function-esc -a action arg1 arg2 arg3 arg4 arg5
  set -a list \
  "h help"                "echo :(" \
  "smcup cursor-mode-on"  "tput smcup" \
  "rmcup cursor-mode-off" "tput rmcup" \
  "getpos cpos"           'printf "\033["' \
  "home"                  'printf "\033[H"' \
  "goto"                  'printf "\033[$arg2;"$arg1"H"' \
  "up"                    'printf "\033["$arg1"A"' \
  "down"                  'printf "\033["$arg1"B"' \
  "right"                 'printf "\033["$arg1"C"' \
  "left"                  'printf "\033["$arg1"D"' \
  "setcolumn"             'printf "\033["$arg1"G"' \
  "clearscreen"           'printf "\033[2J"' \
  "clearline"             'printf "\033[2K"' \
  "show visible cvvis"    'printf "\033[?25h"' \
  "hide invisible civis"  'printf "\033[?25l"' \
  "savepos"               'printf "\033[s\033 7"' \
  "restorepos"            'printf "\033[u\033 8"'

  set help 0
  if not set -q action; or [ -z $action ]; or [ $action = "h" ]; or [ $action = "help" ]
    set help 1
  end
  # echo $action (count $action) (set -q action; and echo set; or echo not set) $help
  # exit 2

  if [ $help = 1 ]
    # set maxlen 0
    # for i in ( seq 1 2 ( count $list ) )
    #   set keys $list[$i]
    #   # set value $list[( expr $i + 1 )]
    #
    #   set len ( string length $keys )
    #   [ $len -gt $maxlen ]; and set maxlen $len
    # end

    for i in ( seq 1 2 ( count $list ) )
      set keys $list[$i]
      set value $list[( expr $i + 1 )]

      echo $keys
    end

  else
    for i in ( seq 1 2 ( count $list ) )
      set keys $list[$i]
      set value $list[( expr $i + 1 )]

      for k in ( string split " " $keys )
        if [ $action = $k ]
          eval $value
          return 0
        end
      end
    end
  end

  return 1

  switch $action
  case 'help'
    echo ":("

  case 'smcup' 'cursor-mode-on'
    tput smcup

  case 'rmcup' 'cursor-mode-off'
    tput rmcup

  case 'get-pos' 'cpos'
    printf "\033["

  case 'home'
    printf "\033[H"

  case 'up'
    printf "\033["$value"A"

  case 'down'
    printf "\033["$value"B"

  case 'right'
    printf "\033["$value"C"

  case 'left'
    printf "\033["$value"D"

  case 'set-column'
    printf "\033["$value"G"

  case 'clear-screen'
    printf "\033[2J"

  case 'clear-line'
    printf "\033[2K"

  case 'show' 'visible' 'cvvis'
    printf "\033[?25h"

  case 'hide' 'invisible' 'civis'
    printf "\033[?25l"

  end
end
