# provides get-list-index

function ticta--lib-function-get-list-index -a index
  if not isatty stdin
    set -l stdin
    cat /dev/stdin | while read --list line; set --append stdin $line; end
    echo $stdin[$index]
  else
    echo $argv[$index]
  end
end
