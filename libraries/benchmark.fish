# provides benchmark

complete -x -c ticta--lib-function-benchmark
function ticta--lib-function-benchmark
  # argparse 'n/tests=' 'm/microseconds' -- $argv
  argparse 'n/tests=' 's/sleep' -- $argv

  set n ( default-val "$_flag_tests" 10 )
  set sleep ( default-val "$_flag_sleep" 0.1 )

  set cmd ( string join " " $argv )

  # if test -n "$_flag_microseconds"
  #   set now_func 'now-μs'
  #   set result_unit 'μs'
  # else
  #   set now_func 'now-ms'
  #   set result_unit 'ms'
  # end

  # test -n "$_flag_quiet"; and set cmd "$cmd > /dev/null"

  set -a results
  for i in ( seq 1 $n )
    set -l start ( ticta--function-now-microseconds )
    eval $argv
    set -a results ( math ( ticta--function-now-microseconds ) - $start )
    sleep $sleep
    # start=($now_func) eval "$argv; set -a results ( math ($now_func) - $start )"
  end

  set min $results[1]
  set max $results[1]
  for result in $results[2..]
    test $result -lt $min; and set min $result
    test $result -gt $max; and set max $result
  end

  set avg ( math \(( string join " + " $results )\) / $n  )

  set result_unit 'μs'
  echo "$n results" ( count $results ) $results
  for var in min max avg
    echo "$var: $$var$result_unit"
  end
end
