# provides sdict Simple dictionary, stores key/value pairs as an array
# provides sdict-print

set -g ticta__sdict_0_arg_verbs 'quiet'
set -g ticta__sdict_1_arg_verbs 'get' 'delete' 'incr' 'decr' 'echo'
set -g ticta__sdict_2_arg_verbs 'set' 'append' 'prepend'

set -g ticta__sdict_seperator \x01\x01

set -al complete_argsets \
  '-s "q" -l "quiet" -d "Disables echoing dict at the end"' \
  '-l "get" -d "sets \$dict_\$arg with value from dict"' \
  '-l "delete" -d "deletes key"' \
  '-l "incr" -d "increments key"' \
  '-l "decr" -d "decrements key"' \
  '-l "echo" -d "echos key. implies --quiet"' \
  '-l "set" -d "sets key to value"' \
  '-l "append" -d "appends to key\'s value"' \
  '-l "prepend" -d "prepends to key\'s value"'

for argset in $complete_argsets
  eval complete -x -c ticta--lib-function-sdict $argset
end

function ticta--lib-function-sdict -S
  set -l target_variable

  set -l dict ""
  set -l array
  if not isatty stdin
    set -l stdin
    cat /dev/stdin | while read line; set --append stdin "$line"; end
    set array ( echo -n -- $stdin | string split $ticta__sdict_seperator )
    # for line in $stdin
    #   set -a array $line
    # end
  else if string match --invert -- '-*' $argv[1]
    set target_variable $argv[1]
    set argv $argv[2..]

    set array ( echo -n -- $$target_variable | string split $ticta__sdict_seperator )
  else
    # echo "sdict either accepts a piped input or a variable name as the first argument"> /dev/stderr
    # return 1
  end

  if test -z "$array" -o ( count $array ) -eq 1
    set array
  end

  set -l is_being_piped 0

  if test -t 1
    # echo 'stdout!' > /dev/stdout
  else
    # echo 'pipe!!!!1' > /dev/stdout
    set is_being_piped 1
  end

  set -l quiet 0

  set -l verb ''
  set -l args_n 0
  set -l args

  for arg in $argv
    # echo "arg: $arg, current verb: $verb, current_args: $args, expected: $args_n"
    set -l is_verb ( string-startswith '--' $arg; and echo 1; or echo 0 )
    if test -z "$verb"
      if test $is_verb -eq 0
        echo "sdict unknown input: $arg" > /dev/stderr
        return 1
      end
      set verb ( string sub --start 3 -- "$arg" )
      switch $verb
      case $ticta__sdict_0_arg_verbs
        set args_n 0
      case $ticta__sdict_1_arg_verbs
        set args_n 1
      case $ticta__sdict_2_arg_verbs
        set args_n 2
      case '*':
        echo "sdict unknown verb: $verb" > /dev/stderr
        return 1
      end

      if test $args_n != "0"
        continue
      end
    end

    if test -n "$verb" -a $args_n != "0"
      set -a args $arg
    end

    if test -n "$verb" -a ( count $args ) -eq $args_n
      # echo "running verb $verb with args ["( string join ', ' -- $args )"]"

      set -l array_size ( count $array )
      set -l index -1
      set -l val_index -1
      if test $args_n != "0"
        for i in ( seq 1 2 $array_size )
          if test $array[$i] = $args[1]
            set index $i
            continue
          end
        end
      end

      if test $index -eq -1; contains $verb $ticta__sdict_2_arg_verbs
        set index ( math $array_size + 1 )
        set -a array "$args[1]"
      end
      set -l val_index ( math $index + 1 )

      switch $verb
      case "q" "quiet"
        set quiet 1
      case "get"
        # echo $array[$val_index]
        set -l varname "dict_$args[1]"
        set -x $varname $array[$val_index]
      case "delete"
        set -e array[$val_index]
        set -e array[$index]
      case "incr"
        set array[$val_index] ( math $array[$val_index] + 1 )
      case "decr"
        set array[$val_index] ( math $array[$val_index] - 1 )
      case "echo"
        test $quiet -eq 0; and set quiet 1
        echo $array[$val_index]

      case "set"
        set array[$val_index] $args[2]
      case "prepend"
        set array[$val_index] "$args[2]""$array[$val_index]"
      case "append"
        set array[$val_index] "$array[$val_index]""$args[2]"
        # set "--$verb" array[$val_index] $args[2]
      end
      set verb ""
      set args_n 0
      set args
    end
  end

  if test -n "$target_variable"
    set -g $target_variable $array
  end

  if test $quiet = "0"
    if test $is_being_piped = '1'
      echo -e ( string join $ticta__sdict_seperator $array )
    else
      ticta--lib-function-sdict-print $array
    end
  end

end

function ticta--lib-function-sdict-print
  set -l split ( string split $ticta__sdict_seperator $argv )
  for i in ( seq 1 2 ( count $split ) )
    set -l val_i ( math $i + 1 )
    echo -e $brblue$bold"\"$split[$i]\""$normal" = "$brgreen"\"$split[$val_i]\""
  end
end
