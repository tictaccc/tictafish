#!/bin/bash

spinner_list=('|' '/' '-' '\\')
spinner_n=${#spinner_list[@]}
clear_line='\0'

color() {
  printf "\033[38;5;${1}m"
}

move_up() {
  printf "\033[${1}A"
}

bars=(
  -999 $(color 160)'▂'
  -90 $(color 208)'▃'
  -75 $(color 226)'▅'
  -60  $(color 156)'▆'
  -45  $(color 118)'▇'
)

# increase_str=$(color 118)'↑'
# decrease_str=$(color 160)'↓'
# same_str=$(color 32)'-'

increase_str=$(color 118)'▲'
decrease_str=$(color 160)'▼'
same_str=$(color 32)'·'



# function _exit --on-job-exit %self
#   notify-send 'heyyyy'
#   echo interrupt!!
# end

# status --is-interactive
trap 'echo -e \\n; tput cvvis; tput sgr0' EXIT
# trap -p

i=0
last_strength=0
n=0
interfaces_n=0

tput civis
while true; do
  sleep 0.2
  strengths_str=$(\
    iwconfig 2>&1 \
    | perl -p -e 's/\n+/;/g' \
    | perl -p -e 's /(^|;)[a-zA-Z0-9]+.*no wireless extensions\.;//g' \
    | perl -p -e 's/(;+|^)([a-zA-Z0-9]+).*?Signal level=-([0-9]{1,3}) dBm.+?(?=;;)/\2,\3,0,/g' \
    | perl -p -e 's/,;+$//' \
  )

  IFS=',' read -a strengths <<< $strengths_str

  [ $n = 0 ] && n=${#strengths[@]} && interfaces_n=$(($n / 3))

  # strength=$(\
  #   iwconfig 2>&1 \
  #   | grep -oh 'Signal level=\-.. dBm' \
  #   | grep --color=never -o '\-..' \
  # )

  spinner_char=$(calc "($i % $spinner_n)")
  echo -e "\r"${spinner_list[$spinner_char]}""

  for strengths_i in $(seq 0 3 $(($n - 1))); do
    interface=${strengths[$strengths_i]}
    strength=${strengths[$(($strengths_i + 1))]}
    last_strength=${strengths[$(($strengths_i + 2))]}

    echo "$interface, $strength, $last_strength"
    continue

    echo -en "\r"$(tput sgr0)

    echo -en "$interface "

    echo -en "-$strength"


    [ "$strength" -eq "$strength" ] 2> /dev/null || strength=-999

    if [ $strength -eq $last_strength ]; then
      echo -en $same_str" "
    elif [ $strength -gt $last_strength ]; then
      echo -en $increase_str" "
    else
      echo -en $decrease_str" "
    fi

    echo -en $(tput sgr0)
    echo -en "dBm "


    for j in $(seq 0 2 9); do
      # echo -n ${bars[$j]}
      # [ $strength -gt ${bars[$j]} ] && echo yes || echo no
      if [ $strength -gt ${bars[$j]} ]; then
        echo -en ${bars[$((j+1))]}
      else
        break
      fi
    done
    echo -en " "
    tput el
    echo
    # last_strength=$strength
    strengths[$(($strengths_i + 2))]=$strength
  done

  i=$((i+1))
  move_up $(($interfaces_n + 1))
done
