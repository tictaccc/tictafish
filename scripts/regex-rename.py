#!/usr/bin/python3

from collections import OrderedDict
import traceback
import argparse
import math
import sys
import re
import os

def walk():
    for root, subdirs, files in os.walk(os.getcwd()):
        for file in files:
            yield os.path.relpath(os.path.join(root, file))

def find(args):
    if args.regex_match:
        try:
            regexp = re.compile(
                args.file_match,
                flags=(re.IGNORECASE if args.case_insensitive else 0)
            )
        except:
            traceback.print_exc()
            print('Error compiling regular expression :(')
            sys.exit(1)

        file_generator = filter(regexp.match, walk() if args.recursive else os.listdir())
    else:
        import glob
        file_generator = glob.iglob(
            args.file_match,
            recursive=args.recursive
        )

    def print_prog(n):
        print(f'\rDiscovered {n} files', end='')

    files = []
    n     = 0
    for file in file_generator:
        if os.path.isfile(file) or args.match_directories:
            files.append(file)

        n += 1
        if ((not args.hide_progress)
             and ((n < 100 and n % 10 == 0)
               or (n < 1000 and n % 50 == 0)
               or (n % 100 == 0))):

            print_prog(n)

    if not args.hide_progress:
        print_prog(n)
        print()

    return files


def create_map(args, files):
    res = OrderedDict()
    for path in files:
        dir, fn = os.path.split(path)
        subbed_fn = args.pattern.sub(args.replacement, fn)
        if fn == subbed_fn:
            continue

        res[path] = os.path.join(dir, subbed_fn)

    return res


def main():
    parser = argparse.ArgumentParser(
        description=(
            'Batch renames files using regex replacement for file names. '
            'Ignores directories by default.'
        )
    )


    flags = [
        ('d', 'match-directories', 'Allow matching and renaming/copying directories.'),
        ('e', 'regex-match',       'Use a regex initial file match instead of glob'),
        ('r', 'recursive',         'Allow recursive file matching, (e.g. **/*.js)'),
        ('y', 'yes',               'Don\'t confirm before renaming'),
        ('c', 'copy',              'Copy instead of moving files'),
        ('p', 'hide-progress',     'Don\'t print progress bar'),
        ('i', 'case-insensitive',  'Case insensitive'),
    ]
    for short, long, help in flags:
        parser.add_argument(
            f'-{short}', f'--{long}',
            action ='store_true',
            help   = help
        )

    parser.add_argument('file_match',  type=str)
    parser.add_argument('pattern',     type=str)
    parser.add_argument('replacement', type=str)

    args = parser.parse_args()

    if args.copy:
        import shutil

    try:
        args.pattern = re.compile(args.pattern)
    except:
        traceback.print_exc()
        print('Error compiling pattern :(')
        sys.exit(1)

    prog_verb_present = 'copy' if args.copy else 'rename'
    prog_verb_past    = 'copied' if args.copy else 'renamed'

    files  = find(args)
    if not files:
        print(f'No files match \'{args.file_match}\' :(')
        sys.exit(1)

    fn_map = create_map(args, files)
    if not fn_map:
        print(f'No files to {prog_verb_present} :(')
        sys.exit()

    for fn in sorted(fn_map, key=os.path.split):
        new_fn = fn_map[fn]
        print(fn, '->', new_fn)

    try:
        if not args.yes and input(f'{prog_verb_present.capitalize()} {len(fn_map)} files? (Y/n) ').lower() not in ['', 'y', 'yes']:
            sys.exit(1)
    except KeyboardInterrupt:
        print()
        sys.exit(1)

    n   = len(fn_map)
    pad = len(str(n))
    progress = ''

    prog_len       = 10
    prog_len2      = prog_len * 2
    prog_char      = '━'
    prog_half_char = '╸'

    for i, fn in enumerate(fn_map, start=1):
        if not args.hide_progress:
            p = int((i/n) * prog_len2)
            chars = math.floor(p / 2)
            half  = prog_half_char if p % 2 == 1 else ' '

            if i == n:
                progress = f' [{prog_char * prog_len}]'
            else:
                # progress = (
                #     ' ['
                #     + ("-" * (chars - 1))
                #     + (">" if chars else "")
                #     + (' ' * (prog_len - chars))
                #     + ']'
                # )
                progress = (
                    ' ['
                    + (prog_char * chars)
                    + half
                    + (' ' * (prog_len - chars - 1))
                    + ']'
                )

        try:
            if args.copy:
                shutil.copy(fn, fn_map[fn])
            else:
                os.rename(fn, fn_map[fn])

            print(f'\r{prog_verb_past} {i:0>{pad}}/{n}{progress}', end='')

        except Exception as e:
            clear_line = '\x1b[0K'
            print(f'\r{clear_line}Couldn\'t {prog_verb_present} {fn}: {e}')

    print()

if __name__ == '__main__':
    main()
