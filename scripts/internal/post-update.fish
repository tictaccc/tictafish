#!/usr/bin/env -S TICTA_MINIMAL=1 /usr/bin/fish

if set -qU tcfg_accent_color; and not set -qU tcfg_color_accent
  set -U tcfg_color_accent $tcfg_accent_color
  set -eU tcfg_accent_color
end

if set -qU tcfg_tertiary_color; and not set -qU tcfg_color_tertiary
  set -U tcfg_color_tertiary $tcfg_tertiary_color
  set -eU tcfg_tertiary_color
end
