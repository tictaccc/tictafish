
# set TRUE "$bold$brgreen""true""$unbold$normal"
# set FALSE "$bold$brred""false""$unbold$normal"

complete -x -c ticta-cfg -n "__fish_seen_subcommand_from get" -s "d" -l "default-value" -d "get default value instead of user value"
complete -x -c ticta-cfg -n "__fish_seen_subcommand_from get" -s "p" -l "prop" -d "get specified prop value instead of value"
# complete -x -c ticta-cfg -n "__fish_seen_subcommand_from get; and __fish_seen_argument -- -p --prop" -a "type" -a "description" -a "private" -a "on_change" -a "unit"
function ticta--cfg-get -a name fallback
  argparse 'd/default-value' 'prop=' -- $argv
  echo $argv | read --local name fallback
  if test -z "$name"
    echo "usage: ticta--cfg-get [-d/default-value] [--prop=prop name] \$name \$fallback, \$name can't be empty" > /dev/stderr
    return 1
  end

  if test -n "$_flag_prop"
    set prop_varname "tdcfg__""$_flag_prop""_$name"
    set prop_value $$prop_varname
    switch "$_flag_prop"
    case 'description'
      ticta-lib format-string "$prop_value"
    case '*'
      echo $prop_value
    end
    return
  end

  set varname tcfg_$name
  set default_varname tdcfg_$name

  if test -n "$_flag_default_value"
    echo $$default_varname
    return ( set -q $default_varname )
  end

  if set -q $varname
    echo $$varname
    return ( set -q $varname )
  else if test -n "$fallback"
    echo $fallback
    return 0
  else if set -q $default_varname
    echo $$default_varname
    return ( set -q $default_varname )
  else
    return 1
  end
end

function ticta--cfg-assert
  argparse 'i/invert' 'n/not' 'm/match=' -- $argv
  set name $argv

  set value ( ticta--cfg-get $name)
  if test -z "$_flag_match"
    set value ( tictabox bool $value )
    set match 1
  else
    set match $_flag_match
  end


  if test "$value" = "$match"
    # assertion success
    if set -q _flag_invert; or set -q _flag_not
      return 1
    end
    return 0
  else
    # assertion fail
    if set -q _flag_invert; or set -q _flag_not
      return 0
    end
    return 1
  end
end

complete -x -c ticta-cfg -n "__fish_seen_subcommand_from set" -s "s" -l "save" -d "make change persistant"
complete -x -c ticta-cfg -n "__fish_seen_subcommand_from set" -s "f" -l "force" -d "ignore private prop"
complete -x -c ticta-cfg -n "__fish_seen_subcommand_from set" -l "implicit-bool" -d "suppresses warning on non-explicit bool value"
complete -x -c ticta-cfg -n "__fish_seen_subcommand_from set" -l "no-on-change" -d "don't run on_change script"
function ticta--cfg-set
  argparse 'e/erase' 's/save' 'x/export' 'f/force' 'implicit-bool' 'no-on-change' -- $argv
  set -l name $argv[1]
  set -l value $argv[2..]

  set force_prompt "To ignore this and set it anyway, run this command again with the -f/--force flag. (this could break stuff!!1)"

  # set -l type_varname tdcfg__type_$name
  # set -l on_change_varname tdcfg__on_change_$name

  set -l var_type ( ticta--cfg-get --prop type $name )
  set -l var_on_change ( ticta--cfg-get --prop on_change $name )

  set -l run_on_change ( test -z "$_flag_no_on_change" -a -n "$var_on_change"; and echo 1; or echo 0 )

  set varname tcfg_$name

  if test -z "$_flag_force"
    # set -l private_varname tdcfg__private_$name
    if tictabox assert-bool ( ticta--cfg-get --prop private $name )
      echo "$name is a private variable, meant to be set internally by another tictafish function." > /dev/stderr
      echo $force_prompt > /dev/stderr
      return 1
    end
  end

  if test -n "$_flag_erase"
    set -e $varname
    return 0
  end

  switch $var_type
  case "number" "integer"
    if not tictabox is-number "$value"
      echo "$name is type $$type_varname, \"$value\" isn't a valid number" > /dev/stderr
      return 1
    end

    if test $var_type = "integer"
      set value ( math "floor($value)" )
    end
  case "boolean"
    if test -z "$_flag_implicit_bool"; and not tictabox is-bool "$value"
      echo "warning: $name is type boolean, value isn't an explicit boolean value ($ticta__bool_falsey or $ticta__bool_truthy)." > /dev/stderr
      echo "To suppress this warning pass the --implicit-bool argument." > /dev/stderr
      echo "Implicitly converted to "( tictabox bool --name "$value" )"." > /dev/stderr
    end
    set value ( tictabox bool "$value" )
  case "color"
    set value ( string lower "$value" )
    set value_hex ( echo $value | perl -pe 's/.*?(?:^(?:#|0x)?([0-9a-f]{6})$)?.*/\1/' ) # great heavens thats a regexp

    if test -z "$value_hex"; and not contains $value $ticta__named_colors
       echo "value \"$value\" doesn't seem to be a named color (\$ticta__named_colors) or a valid 6 digit hex value" > /dev/stderr
       echo $force_prompt > /dev/stderr
       return 1
    else if test -n "$value_hex"
      set value "#$value_hex"
    end
  case "*"
  end

  set -l on_change_OLD_VALUE
  if test $run_on_change -eq 1
    set on_change_OLD_VALUE ( ticta--cfg-get $name )
  end

  if test -n "$_flag_export"
    set -gx $varname $value
  else
    set -g $varname $value
  end

  if test -n "$_flag_save"
    set -ge $varname
    set -Ux $varname $value
  end

  if test $run_on_change -eq 1
    OLD_VALUE="$on_change_OLD_VALUE" VALUE="$value" eval $var_on_change
  end

  return 0
end

function ticta--cfg-list
  argparse 'a/include-non-default' -- $argv
  echo $argv | read --local mode argv

  if test -n "$_flag_include_non_default"
    set grep_args "^td?cfg_[^_]"
  else
    set grep_args "^tdcfg_[^_]"
  end

  set ticta_vars ( set | grep -E $grep_args | sed -E 's/td?cfg_([a-zA-Z_]+).*/\1/' )

  switch "$mode"
  case 'array'
    echo -en $ticta_vars

  case 'keys' 'novals'
    for name in $ticta_vars
      echo $name
    end

  case 'basic' 'pairs'
    for name in $ticta_vars
      echo $name,( ticta-cfg get $name )
    end

  case "*"
    set maxlen 3 # "Key column label"
    for name in $ticta_vars
      set -l len ( string length $name )
      [ $len -gt $maxlen ]; and set maxlen $len
    end

    set -x space_n ( math $maxlen + 1 )
    function space_str -a key_len char
      test -z "$char"; and set char ' '
      string repeat -n ( math "$space_n - $key_len" ) $char
    end

    for i in ( seq 1 ( count $ticta_vars ) )
      set name $ticta_vars[$i]
      set -l len ( string length $name )

      set -l is_private ( ticta-cfg get --prop private $name )
      if test $is_private -eq 1
        echo -en "$dim$name$normal*"
        set len ( math $len + 1)
      else
        echo -n $name
      end

      set -l value ( ticta-cfg get $name )
      set -l value_default ( ticta-cfg get --default-value $name )

      echo -n ( space_str $len  )
      if test "$value" = "$value_default"
        echo -n "~ "
      else
        echo -n "= "
      end

      set -l type ( ticta-cfg get --prop type $name )
      switch $type
      case 'boolean'
        ticta-lib format bool ( tictabox bool --name $value )
      case 'color'
        test -n "$value"; and ticta-lib format --color-block-left "$type" "$value"
      case 'number' 'int'
        ticta-lib format number $value
      case 'scheme'
        ticta-lib format scheme $value 
      case '*'
        ticta-lib format auto $value
      end

      set -l unit ( ticta-cfg get --prop unit $name )
      test -n "$unit"; and echo -en " $dim""["( ticta-cfg get --prop unit $name )"]"

      echo -e "$normal"
      # set -l description ( ticta-cfg get --prop description $name )
      # if test -n "$description"
      #   echo "  \""( echo $description )\"
      # end
    end
    functions --erase space_str
  end
end

function ticta--cfg-help
  echo "ticta-cfg is just a helper function to get config values"
  # echo "usage: ticta-cfg visual"
  # echo "     - ticta-cfg helper"
  # echo
  echo "usage: ticta-cfg get [-d/default-value] <key> [fallback]"
  echo "     - ticta-cfg get c_prompt_hostname cyan: returns \$tcfg_c_prompt_hostname or cyan"
  echo "     - ticta-cfg get c_red: returns \$tcfg_c_red or \$tdcfg_c_red"
  echo
  echo "usage: ticta-cfg assert <key>"
  echo "     - ticta-cfg assert enable_nice_tty"
  echo
  echo "usage: ticta-cfg set/export/save <key> <value>"
  echo "     - ticta-cfg save c_red ff0000"
  echo
  echo "usage: ticta-cfg erase <key>"
  echo "     - ticta-cfg erase color_tmux_status"
  echo
  echo "usage: ticta-cfg list [mode]"
  echo "     - ticta-cfg list"
  echo "     - ticta-cfg list array"
  echo "     - ticta-cfg list keys/novals"
  echo "     - ticta-cfg list basic/pairs"
  return 1
end


function ticta-cfg -d "Get user provided or default config value"
  echo $argv | read --local verb argv
  # set verb ( echo $verb | string lower )
  set argv ( echo $argv | string split ' ' )

  switch $verb
  # case "visual" "helper"
  #   ticta--visual-cfg

  case "get" "assert" "set"
    ticta--cfg-$verb $argv

  case "export"
    ticta--cfg-set --export $argv

  case "save"
    ticta--cfg-set --export --save $argv

  case "erase"
    ticta--cfg-set --erase $argv

  case "list"
    ticta--cfg-list $argv

  case "" "help" "*"
    ticta--cfg-help
  end

  return $status
end

set -g ticta__cfg_verbs "get assert set export save erase list"
set -g ticta__cfg_key_autocomplete_verbs "get assert set export save erase"
function root_complete
  complete -x -c ticta-cfg -n "not __fish_seen_subcommand_from $ticta__cfg_key_autocomplete_verbs" $argv
end
root_complete -a get    -d "get config value"
root_complete -a assert -d "return status code based on config value"
root_complete -a set    -d "set config value"
root_complete -a erase  -d "erase config value"
root_complete -a export -d "export config value"
root_complete -a save   -d "export and save config value"
root_complete -a list   -d "list all config values"
functions -e root_complete

### See config.fish for updating completion for individual keys!!

function list_complete
  complete -x -c ticta-cfg -n "__fish_seen_subcommand_from list" $argv
end
list_complete -s a -l include-non-default -d "Include config keys not from 'default-config.fish'"
list_complete -a array -d "\$IFS seperated list of config keys"
list_complete -a keys  -d "newline seperated list of config keys"
list_complete -a pairs -d "newline seperated list key,val pars"
functions -e list_complete
