
# set -g ticta__all_functions ( cat **.fish | grep -E '^[^#]*tictabox-register' | perl -pe 's/.*tictabox-register (\S+).*/\1/g' | string split \n )
set -g ticta__functions_names
set -g ticta__functions

function tictabox-register
  argparse "d/desc=" "n/no-alias" -- $argv
  echo $argv | read name argv

  set -l internal_func_name "ticta--function-$name"

  if not functions -q $internal_func_name
    echo "tictabox-register called for name $name but function $func_name doesn't exist" > /dev/stderr
    return 1
  end

  set -ag ticta__functions_names "$name"
  set -ag ticta__functions "$name,$_flag_desc"

  if test -n "$_flag_desc"
    # complete -x -c $internal_func_name -d "$_flag_desc"
    functions -d "$_flag_desc" $internal_func_name
  end

  # complete -c "tictabox" -n "__fish_seen_subcommand_from $name" -w $func_name
  # complete -x -c "tictabox" -n "complete -e tictabox && complete -x -c tictabox -w \"$name\"" -a 'lol'
  if status is-interactive
    set complete_script ( \
      complete -c $internal_func_name \
      | grep -v -- "--wraps" \
      | string replace "$internal_func_name" "tictabox -n '__fish_seen_subcommand_from $name'"\
      # | perl -p -e "s/$internal_func_name/tictabox -n '__fish_seen_subcommand_from $name'/g" \
    )
    for line in $complete_script
      eval $line
    end
  end

  if test -z $_flag_no_alias; and not functions -q $name; and not set -q $name
    status is-interactive; and complete -c $name -w $internal_func_name

    set -l details ( functions $internal_func_name )
    set -l original_opts ( echo $details[2] | sed -E "s/.*function $internal_func_name (.*)/\1/" )

    set -l opts ( echo $original_opts | grep -i -- '--no-scope-shadowing' &> /dev/null; and echo '--no-scope-shadowing' )

    function $name -V internal_func_name --description "$_flag_desc" $opts
      $internal_func_name $argv
    end
  end
end

function tictabox -a func_name
  set func_name ( echo $func_name | string lower )
  if test -z "$func_name"; or contains $func_name h help
    echo 'Usage: function ...'
    for func in $ticta__functions
      echo $func | read -d ',' name desc
      echo -n "  $bold$name$unbold"
      test -n "$desc"; and echo -n ": $desc"
      echo
    end
    return 255
  end

  for name in $ticta__functions_names
    if test $name != $func_name
      continue
    end

    set -l func_name "ticta--function-$name"
    set -l argstring
    for arg in $argv
      set argstring $argstring \"$arg\"
    end
    eval $$func_name $argstring
    return $status
  end
  echo 'unknown function :('
  return 127
end

function tictabox--list-completions
  complete -c tictabox | perl -pe 's/(.*)-n \'\w+ (\S+)\'/subcommand \2: \1/g'
end

for fn in $ticta__dir/functions/**.fish
  source $fn
end

for func in $ticta__functions
  echo $func | read -d ',' name desc
  complete -x -c tictabox -n "not __fish_seen_subcommand_from $ticta__functions_names" -a "$name" -d "$desc"
end
