register-test "expected output" \
  --expect-status 0 \
  --expect-output "hello, world!" \
  "echo 'hello, world!'"

register-test "expected status" \
  --expect-status 1 \
  "false"