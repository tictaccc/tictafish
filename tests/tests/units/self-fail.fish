register-test "expected output (should fail)" \
  --expect-status 1 \
  --expect-output "not hello, world!" \
  "echo 'hello, world!'"

register-test "expected status (should fail)" \
  --expect-status 32 \
  "false"