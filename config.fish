# DONT EDIT THIS!! It's supposed to be updated in git pulls
#     vvvvvvvvvvvvvvvvvvvvvvvv
# put `set tcfg_VARNAME value` in your main config.fish
#     ^^^^^^^^^^^^^^^^^^^^^^^^
# above `source $HOME/.config/fish/tictafish/ticta.fish`

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# prompt (tcfg_prompt) and color scheme settings are stored in    #
# ~/.config/fish/fish_variables. Set them with the                #
# `ticta-prompt-use` and `ticta-scheme-use` commands respectively #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

if not set -q ticta__cached_config_file
  set -g ticta__cached_config_file "$ticta__runtime_dir/ticta-config.fish"
  # if set -q TERMUX_VERSION
  #   set -g ticta__cached_config_file "$ticta__termux_root/usr/var/run/ticta-config-"(id --user)".fish"
  # else
  # end
end

complete -x -c ticta--update-config-file -d "Update cached config file"
function ticta--update-config-file
  set -l fn $ticta__cached_config_file

  echo '#!/usr/bin/fish' > $fn
  echo '# WARNING! Automatically generated by ticta.fish. Any modifications to this file will be lost!!' >> $fn

  set types "boolean" "color" "integer" "list" "number" "scheme" "string" 
  set sed_pattern ''
  set indent_rule
  set indent_rule_sub_n

  set var_name ""
  set default_value ""

  set props "type" "description" "private" "on_change" "unit"

  set prop__default_type "string"
  set prop__default_private "0"

  function reset-props -S
    for prop in $props
      set -l default_varname "prop__default_$prop"
      if set -q $default_varname
        set -x "prop_$prop" "$$default_varname"
      else
        set -x "prop_$prop" ""
      end
    end
    # set default_value ""
    # set type "string"
    # set description ""
    # set private 0
    # set on_change ""
    # set unit ""
  end
  reset-props

  for line in ( cat $ticta__dir/config.txt )
    if test -z ( string trim -- "$line" ); or string-startswith '#' ( string trim --left $line )
      continue
    end
    if test -z "$indent_rule"
      set -l match ( echo $line | sed -E 's/^(\s+)?.*/\1/' )
      if test -n $match
        set indent_rule $match
        set indent_rule_sub_n ( math ( string length $match ) + 1)
      end
    end

    if test -n "$indent_rule"; and string-startswith "$indent_rule" "$line"
      string sub --start $indent_rule_sub_n $line | read --local prop value
      if not contains $prop $props
        echo "unknown config.txt prop: $prop" > /dev/stderr
        continue
      end

      switch $prop
      case "type"
        if not contains $value $types
          echo "unknown config.txt prop type: \"$value\". defaulting to string" > /dev/stderr
          set value "string"
        end
      case "private"
        set value ( tictabox bool $value )
      case "description"
        set value ( echo $value | perl -pe 's/(\'|")/\\\\\1/g' )
        set value ( ticta-lib format-string "$value" )
      end

      set "prop_$prop" $value
      continue

    else
      ### Non-indented line

      if test -n $var_name
        ### $var name will be set if we just left an indented block
        set -l varname "tcfg_$var_name"
        set -l varname_default "tdcfg_$var_name"

        if set -q $varname
          set current_value "$$varname"
        else if set -q $varname_default
          set current_value "$$varname_default"
        end

        echo set -g "$varname_default" ( string escape -- "$default_value" ) >> $fn

        for prop in $props
          set -l prop_varname "prop_$prop"
          echo set -g "tdcfg__$prop""_$var_name" ( string escape -- "$$prop_varname" ) >> $fn
        end
        set -l description "<$prop_type>"
        if set -q $varname_default
          set -a description "default=\'$$varname_default\'"
        end

        ### Since this file is only generated once per session, this would be incorrect after changing any config values.
        # if set -q $varname
        #   set -a description "value=\'$$varname\'"
        # end

        if test -n "$prop_description"
          set description "$description: $prop_description"
        end

        echo -n "complete -x -c ticta-cfg -n \"__fish_seen_subcommand_from get assert set export save erase\" -a $var_name -d '$description'" >> $fn
        echo >> $fn
      end
      set -l groups ( echo $line | perl -pe 's/^([\w_]+)(?::(\w+))?(\s+.+)?$/\1\x1\2\x1\3/' )

      set i 1
      for group in ( string split \1 $groups )
        switch $i
        case 1
          set var_name $group
          reset-props

        case 2
          if test -n "$group"
            set group ( string trim --left --char ":" $group )
            if not contains $group $types
              echo "unknown config.txt prop type: \"$value\". defaulting to string" > /dev/stderr
              set prop_type "string"
            else
              set prop_type $group
            end
          end

        case 3
          set default_value ( string trim --left $group)
        end

        incr i
      end
    end

  end
  functions -e reset-props
end

# set -l modified ( stat $ticta__dir/config.txt | grep -i modify | string sub --start 9 )
# if test \( ! -e "$ticta__cached_config_file" \) -o "$modified" != "$ticta__cached_config_modified_time"
#   echo "Updating cached config file..."
#   ticta--update-config-file
#   set -U ticta__cached_config_modified_time $modified
# end

# source $ticta__cached_config_file
