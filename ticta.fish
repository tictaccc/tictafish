if set -q TICTA_DISABLED
  exit 0
end

set -gx TICTA_VERSION 1.2.0

# Needed for updater
set -eg ticta__initialized
set -gx ticta__recursion_n ( math $ticta__recursion_n + 1 )
set -l ticta__update_script $ticta__dir/scripts/internal/updater.fish

set ticta__dir ( status current-filename | path resolve | path dirname )

source "$ticta__dir/ticta-log.fish"

ticta--debug -v ticta__dir ticta__recursion_n

function ticta-updater
  ticta__dir=$ticta__dir fish -N $ticta__dir/scripts/internal/updater.fish $argv
  return $status
end

if not test -x $ticta__update_script
  chmod +x $ticta__update_script
end

function ticta-init
  function ticta-init--print-help
    echo "usage: ticta-init [-h/--help] [-f/--force]"
    echo "  -f, --force: re-run initialization even if \$ticta__initialized is already set"
    echo
    echo "runtime variables:"
    echo "  `1` is enabled/true, any other value or unset is disabled/false"
    echo "  TICTA_DISABLED:    Fully disable tictafish, this file will immediately exit."
    echo "  TICTA_MINIMAL:     Only initializes minimal variables, for updater.fish when set to 1" 
    echo "  TICTA_RUNTIME_DIR: Directory for storing inter-session but not persistant data."
    echo "                     Usually /var/run/user/$UID/ (/var/run/user/\$UID/)" 
    echo "  TICTA_FUN_SPLASH:  Runs ticta--fun-splash on greeting when set to 1" 
    echo "  TICTA_DEBUG:       Enables some debugging features."
    echo "  TICTA_VERBOSITY:   Sets ticta--log verbosity. 0 (least), 1, 2, or 3 (most)"
    echo "  TICTA_TRACE:       Enables function call stack in log messages"
  end

  function ticta-init--get-tmp -V ticta__termux_root
    if set -q TMPDIR
      echo "$TMPDIR"
    else if set -q TERMUX_VERSION
      echo "$ticta__termux_root"
    else
      echo "/tmp"
    end
  end

  function ticta-init--get-runtime
    if test -n "$TICTA_RUNTIME_DIR"
      if not test -O "$TICTA_RUNTIME_DIR"
        set path "/var/run/user/"( id --user )"/"
        echo "warning: \$TICTA_RUNTIME_DIR is set but the current user doesn't own it :(" > /dev/stderr
        echo "using default runtime directory. (\$path)" > /dev/stderr
      else
        set path "$TICTA_RUNTIME_DIR"
      end
    end

    if not test -O "$path"
      set path "$ticta__tmp_dir/ticta_runtime_dir_user_"( id --user )
    end

    set path "$path/$TICTA_VERSION"
    mkdir -p "$path"
    echo "$path"
  end

  argparse 'h/help' 'f/force' -- $argv

  if set -q _flag_help
    ticta-init--print-help
    return 127
  end

  if not set -q _flag_force; and set -q ticta__initialized
    return 0
  end

  set -e splash_trace_started
  set -e splash_trace_old_verbosity
  if test "$TICTA_SPLASH_TRACE" = "1"; and tput smcup
    printf '\e[1000B'
    set splash_trace_started
    set splash_trace_old_verbosity "$TICTA_VERBOSITY"

    set -g TICTA_VERBOSITY 3
  end

  ticta--debug initializing
  ticta--debug -v TICTA_DEBUG TICTA_VERBOSITY ticta__interactive
  set -g ticta__initialized
  set -g ticta__cvvis ( tput cvvis )

  set -g ticta__tmp_dir ( ticta-init--get-tmp )
  ticta--debug -v ticta__tmp_dir

  set -g ticta__runtime_dir ( ticta-init--get-runtime )
  ticta--debug -v ticta__runtime_dir

  set -l dim ( tput dim )
  set -l normal ( tput sgr0 )
  set -l clearline ( printf '\033[2K' )
  set -l updated 0

  if test -w /dev/stdout
    function ticta-init--print-status -S
      echo -en "\r$clearline$dim$argv$normal" > /dev/stdout
    end
  else
    function ticta-init--print-status -S
    end
  end

  ticta-init--print-status "Starting ticta.fish..."

  set -g ticta__update_notified 0
  
  set -g _shell_spawn_time (date +"%s")

  if set -q TICTA_MINIMAL
    echo -en \r$clearline
    exit 0
  end

  # On non-interactive shells this speeds up initialization a lot by not actually
  # setting any completion rules.
  if not status is-interactive
    function complete
    end
  end

  if test "$ticta__updated" = "1"
    ticta-init--print-status "Running post tictafish update script..."
    set -U ticta__post_update_ran 1
    fish $ticta__dir/scripts/internal/post-update.fish
    # echo "Done! :)"
  end

  ticta--source $ticta__dir/ticta-functions.fish
  ticta--source $ticta__dir/ticta-lib.fish

  ticta--source $ticta__dir/config.fish

  if test "$ticta__updated" = "1" -o ! -e $ticta__cached_config_file
    ticta-init--print-status "Updating cached config file..."
    ticta--update-config-file
  end

  if test "$ticta__updated" = "1" -o ! -e $ticta__cached_lib_completion_file
    ticta-init--print-status "Updating cached library function completions..."
    ticta--lib-update-completions
  end

  ticta-init--print-status

  if test "$ticta__updated" = "1"
    set -U ticta__updated 0
  end

  ticta--debug "sourcing cached config ($ticta__cached_config_file)"
  ticta--source $ticta__cached_config_file
  ticta--source "$ticta__dir/ticta-cfg.fish"

  # Terminal Decorations
  ticta--source "$ticta__dir/parts/colors.fish"
  ticta--source "$ticta__dir/parts/text_styles.fish"

  # Utilities
  ticta--source "$ticta__dir/parts/cd.fish"
  ticta--source "$ticta__dir/parts/dynamic_window_title.fish"
  ticta--source "$ticta__dir/parts/exclaimation.fish"
  
  # Personalization
  ticta--source "$ticta__dir/parts/fun_splash.fish"
  ticta--source "$ticta__dir/parts/greetings.fish"
  ticta--source "$ticta__dir/parts/motd.fish"
  ticta--source "$ticta__dir/parts/prompt.fish"
  ticta--source "$ticta__dir/parts/shell_integrations.fish"

  if status is-interactive
    ticta--source $ticta__cached_lib_completion_file
  else
    functions -e complete
  end

  if not set -q ticta__updater_last_check
    set -U ticta__updater_last_check 0
  end

  if not set -q ticta__updater_up_to_date
    set -U ticta__updater_up_to_date 0
  end

  if status is-interactive \
     && test ( math ( now ) - $ticta__updater_last_check ) -gt 86400
    fish -c 'ticta-updater fetch &> /dev/null & disown &' &> /dev/null & disown
  end

  function ticta--set-greeting
    if ticta-cfg assert --invert enable_greeting
      return
    end

    function fish_greeting
      if test "$TICTA_FUN_SPLASH" = "1"
        ticta--fun-splash
      end

      echo -e \r( ticta--greeting ( ticta-cfg get greeting ) )
      ticta--motd
    end
  end

  function ticta--set-title
    if ticta-cfg assert --invert enable_dynamic_window_title
      return
    end

    function fish_title
      ticta--dynamic-window-title ( string shorten -m 30 -- "$argv" )
    end
  end

  function ticta--set-prompt
    if ticta-cfg assert --invert enable_prompt
      return
    end

    function fish_prompt
      set -l last_status $status
      
      ticta--sync-colors--xterm

      if not type -q ticta--prompt
        echo ( basename $PWD )
        echo -n $yellow
        echo -n $USER
        test "$last_status" != "0"; and echo -n " "$bg_brred$brwhite$last_status
        echo -n $normal
        echo -n ' > '
      else
        set -l lines ( last_status=$last_status ticta--prompt )

        set -l output
        for line in $lines
          set output "$output\n$line"
        end
        echo -en $output
      end

      echo -en "$ticta__cvvis"
    end
  end

  function ticta-reload
    ticta--source $HOME/.config/fish/tictafish/ticta.fish
  end

  if set -q splash_trace_started
    tput rmcup
    set TICTA_VERBOSITY "$splash_trace_old_verbosity"
  end

  ticta--set-greeting
  ticta--set-title
  ticta--set-prompt
end


if status is-interactive; or set -q TICTA_INTERACTIVE
  set -g ticta__interactive
else
  set -eg ticta__interactive
end

if status is-interactive; and not set -q TICTA_DISABLED
  ticta-init
end