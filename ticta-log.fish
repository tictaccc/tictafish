# logging function and related wrappers

set ticta__log_c_level ( tput dim ) ( set_color cyan ) ( set_color green ) ( set_color yellow ) ( set_color brred ) 
set ticta__log_c_var_name ( set_color brblue )
set ticta__log_c_var_value ( set_color brgreen )

function ticta--log
  # longest subject length
  set -l len ( count $argv )

  set -l levels debug verbose info warn error

  set -l level 3
  set -l scopes 1
  set -l subject
  set -l message
  while set -q argv[1]
    switch $argv[1]
      case '-s' '--scopes'
        # number of scopes to show, even if TICTA_TRACE isn't `1`
        set scopes $argv[2]
        set argv $argv[3..]

      case '-l' '--level'
        set -l new_level $argv[2]
        set argv $argv[3..]

        switch "$new_level"
          case 1 2 3 4 5
          case d debug
            set new_level 1
          case v verbose
            set new_level 2
          case l info
            set new_level 3
          case w warn
            set new_level 4
          case e error
            set new_level 5
          case "" "*"
            set new_level 3
        end
        set level $new_level

      case '-v' '--var' '-f' '--flag'
        # set subject "var"
        if not set -q argv[3]
          set -l var $argv[2]
          set -a message "$ticta__log_c_var_name\$$var"
          set -a message "=="
          set -a message "$ticta__log_c_var_value"( string escape -- "$$var" )
        else
          for var in $argv[2..]
            set -a message \n
            set -a message "- $ticta__log_c_var_name\$$var"
            set -a message "=="
            set -a message "$ticta__log_c_var_value"( string escape -- "$$var" )
          end
        end

        break

      case "*"
        break
    end
  end
  
  if test -z "$message"
    set message "$argv"
  end

  set trace
  set i 0
  for func in ( status stack-trace | string trim | string match --regex --groups-only '^(?:in function \'(\S+)\').*$' )
    if contains "$func" "ticta--"{log,debug,verbose,info,warn,error}
      continue
    end
    set i ( math "$i + 1" )
    set -a trace "$func"
    
    if test "$i" -ge "$scopes" -a "$TICTA_TRACE" != "1"
      break
    end
  end
  set trace ( string join " → " -- $trace[-1..1] )

  set -l blue ( set_color blue )
  set -l italic ( tput sitm )
  set -l italic_off ( tput ritm )
  set -l bold ( tput bold )
  set -l dim ( tput dim )
  set -l normal ( set_color normal )

  set output 
  if set -q trace[1]
    set -a output "$blue""[ticta @ $italic$trace$italic_off]"
  else
    set -a output "$blue""[ticta]"
  end

  set -a output "$ticta__log_c_level[$level]["( string upper "$levels[$level]" )"]"

  if test -n "$subject";
    set -a output "$dim$bold<$subject>"
  end
  set -a output "$ticta__log_c_level[$level]"$message # intentional variable expansion
  set -a output "$normal"
  string join "$normal " -- $output > /dev/stderr

end

function ticta--source -S
  source $argv
  set -l __ticta__source_result $status
  ticta--debug "ticta-source: sourcing '$argv'"
  return $__ticta__source_result
end

function ticta--init-logging -v TICTA_VERBOSITY
  function ticta--debug; end 
  function ticta--verbose; end 
  function ticta--info; end

  function ticta--warn
    ticta--log -l warn $argv
  end
  function ticta--error
    ticta--log -l error $argv
  end

  if not status is-interactive
    return 0
  end

  if test "$TICTA_VERBOSITY" -ge 1 &> /dev/null
    function ticta--info
      ticta--log -l info $argv
    end
  end

  if test "$TICTA_VERBOSITY" -ge 2 &> /dev/null
    function ticta--verbose
      ticta--log -l verbose $argv
    end
  end

  if test "$TICTA_VERBOSITY" -ge 3 &> /dev/null
    function ticta--debug
      ticta--log -l debug $argv
    end
  end
end

ticta--init-logging