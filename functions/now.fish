
complete -x -c ticta--function-now
function ticta--function-now
  date +"%s"
end
tictabox-register now -d "Get current time in seconds"

complete -f -c now

complete -x -c ticta--function-now-ms
function ticta--function-now-ms
  date +"%s%3N"
end
tictabox-register now-ms -d "Get current time in milliseconds"

complete -f -c now-ms

complete -x -c ticta--function-now-μs
function ticta--function-now-μs
  date +"%s%6N"
end
tictabox-register now-μs -d "Get current time in microseconds"

complete -x -c ticta--function-now-microseconds
alias ticta--function-now-microseconds='ticta--function-now-μs'
tictabox-register now-microseconds
