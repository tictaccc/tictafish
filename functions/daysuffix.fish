

function ticta--function-daysuffix -a day
  if test -z "$day"
    set day ( date "+%-d")
  end

  if test $day -gt 10 -a $day -lt 20
    echo $day"th"
  else
    switch ( string sub --start -1 $day )
    case 1
      echo $day"st"
    case 2
      echo $day"nd"
    case 3
      echo $day"rd"
    case '*'
      echo $day"th"
    end
  end
end
tictabox-register daysuffix -d "Return suffixed day number"

set -l all_days ( seq 1 31 )
for day in $all_days
  complete -x -c ticta--function-daysuffix \
  # -n "not __fish_seen_subcommand_from $all_days" \
  -a $day \
  -d "the "( ticta--function-daysuffix $day )" day of the month"
end
