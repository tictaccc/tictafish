
function ticta--function-default-val
  for arg in $argv
    if test -n $arg
      echo -n $arg
      return 0
    end
  end
  return 1
end

tictabox-register default-val -d "echos first non-empty argument"
