complete -x -c "ticta--function-incr"
function ticta--function-incr -S
  argparse 'h/help' 'd/decr' -- $argv
  echo $argv | read name k

  if test -n "$_flag_help" -o -z "$name"
    echo 'usage: incr [-d/--decr] <var name> [k]'
    return 255
  end

  test -z "$k"; and set k "1"
  test -n "$_flag_decr"; and set k ( math "$k * -1" )

  set -x $name ( math $$name + $k )
end
tictabox-register incr -d "Increments variable name by k"

complete -x -c "ticta--function-decr"
function ticta--function-decr -S
  ticta--function-incr --decr $argv
end
# alias ticta--function-decr='ticta--function-incr --decr'
tictabox-register decr -d "Alias for incr --decr"
