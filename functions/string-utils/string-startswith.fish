
complete -x -c ticta--function-string-startswith
function ticta--function-string-startswith
  set match "$argv[1]"
  set string ( string join " " -- $argv[2..] )

  if test "$match" = ( string sub -l ( string length -- "$match" ) -- "$string" )
    return 0
  end
  return 1
end

tictabox-register string-startswith -d "Asserts if string starts with another string"
