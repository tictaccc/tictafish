
complete -x -c ticta--function-string-endsswith
function ticta--function-string-endswith
  set match "$argv[1]"
  set string ( string join " " -- $argv[2..] )

  if test "$match" = ( string sub --end -( string length -- "$match" ) -- "$string" )
    return 0
  end
  return 1

end
tictabox-register string-endswith -d "Asserts if string ends with another string"
