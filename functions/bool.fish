
set -g ticta__bool_falsey "0" "false" "f" "no"  "n"
set -g ticta__bool_truthy "1" "true"  "t" "yes" "y"

complete -x -c ticta--function-is-bool
function ticta--function-is-bool -a value
  if contains ( string lower $value ) $ticta__bool_falsey $ticta__bool_truthy
    return 0
  end
  return 1
end
tictabox-register is-bool -d "Asserts if arguemnt is in falsey or truthy list"

# complete -x -c ticta--function-bool -s "f" -l "default-to-true" -d "Should default to true"
complete -x -c ticta--function-bool -s "n" -l "name" -d "echo \"true\" or \"false\" instead of 1 or 0"
complete -x -c ticta--function-bool -s "e" -l "explicit" -d "requires argument to be in \$ticta__bool_falsey or \$ticta__bool_truthy"
function ticta--function-bool
  argparse 'n/name' "e/explicit" -- $argv
  echo $argv | string lower | read --local value

  if test -n "$_flag_explicit"; and not contains $value $ticta__bool_falsey $ticta__bool_truthy
    return 1
  end

  if test -z "$value"; or contains $value $ticta__bool_falsey
    test -n "$_flag_name"; and echo 'false'; or echo '0'
  else
    # test -n "$_flag_default_to_true"; and echo 1; or echo 0
    test -n "$_flag_name"; and echo 'true'; or echo '1'
  end
end
tictabox-register bool -d "echos 1 if argument is in [1, true, yes] else 0"

function ticta--function-assert-bool
  set res ( ticta--function-bool $argv )
  if test "$res" = "1"
    return 0
  else
    return 1
  end
end
tictabox-register assert-bool -d "Wraps bool function and returns instead of echoing output"
